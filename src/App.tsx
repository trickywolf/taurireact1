import {
  
  Grid,
  GridItem,
  HStack,
  
  Show
  
  
} from "@chakra-ui/react";
import NavBar from "./components/NavBar";
import GameGrid from "./components/GameGrid";
import GenreList from "./components/GenreList";
import { useState } from "react";
import { Genre } from "./hooks/useGenres";
import PlatformSelector from "./components/PlatformSelector";
import { Platform } from "./hooks/usePlatforms";
import OrderSelector from "./components/OrderSelector";
import Clock from "./components/Clock";
import DynamicHeading from "./components/DynamicHeading";

export interface GameQuery {
  genre: Genre | null;
  platform: Platform | null;
  order: string;
  searchText: string;
}

function App() {
  //const onClick:(data:string)=>{console.log(data)};
  const [gameQuery, setGameQuery] = useState<GameQuery>({} as GameQuery);
  // const [selectedGenre, setSelectedGenre] = useState<Genre | null>(null);
  // const [selectedPlatform, setSelectedPlatform] = useState<Platform | null>(
  //   null
  // );

  return (
    <>
      <Grid
        templateAreas={{
          base: `"nav" "main"`,
          lg: `"nav nav" "aside main"`,
        }}
        templateColumns={{
          base: "1fr",
          lg: "250px 1fr",
        }}
        // gridTemplateRows={"50px 1fr 30px"}
        // gridTemplateColumns={"150px 1fr"}
        // h="200px"
        // gap="1"
        // color="blackAlpha.700"
        // fontWeight="bold"
      >
        <GridItem pl="2" area={"nav"}>
          <NavBar
            onSearch={(searchText) => {
              setGameQuery({ ...gameQuery, searchText });
              console.log("Search submit:", searchText);
            }}
            
          />
        </GridItem>
        <Show above="lg">
          <GridItem pl="5" area={"aside"}>
            <GenreList
              onSelectGenre={(genre) => setGameQuery({ ...gameQuery, genre })}
              selectedGenre={gameQuery.genre}
            />
          </GridItem>
        </Show>
        <GridItem padding={2} area={"main"}>
          <DynamicHeading gameQuery={gameQuery} />
          <HStack paddingBottom={5} justifyContent={"space-between"}>
            <HStack spacing={5}>
              <PlatformSelector
                onSelectPlatform={(platform) =>
                  setGameQuery({ ...gameQuery, platform })
                }
                selectedPlatform={gameQuery.platform}
              />
              <OrderSelector
                selectedOrder={gameQuery.order}
                onSelectOrder={(order) => setGameQuery({ ...gameQuery, order })}
              />
            </HStack>
            <Clock />
          </HStack>

          <GameGrid gameQuery={gameQuery} />
        </GridItem>
      </Grid>
    </>
  );
}

export default App;
