import { Badge } from "@chakra-ui/react";
import { useEffect, useState } from "react";

const Clock = () => {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <Badge padding={"6px"} fontSize="16px">
      {time.toLocaleTimeString()}
    </Badge>
  );
};

export default Clock;
