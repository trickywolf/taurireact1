import { Badge } from "@chakra-ui/react";

interface Props {
  score: number;
}

const CriticScore = ({ score }: Props) => {
  const color = score > 90 ? "green" : "yellow";
  return (
    <Badge fontSize="14px" colorScheme={color}>
      {score}
    </Badge>
  );
};

export default CriticScore;
