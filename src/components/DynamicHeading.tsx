import { GameQuery } from "../App";

import {
  Heading
} from "@chakra-ui/react";

interface Props {
  gameQuery: GameQuery;
}

const DynamicHeading = ({ gameQuery }: Props) => {
  const headingparts = [
    gameQuery.platform?.name,
    gameQuery.genre?.name,
    "Games",
  ];

  return (
    <Heading as="h1" size="2xl" paddingBottom={5}>
      {headingparts.join(" ")}
    </Heading>
  );
};

export default DynamicHeading;
