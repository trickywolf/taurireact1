import { Card, CardBody, Heading, Image, HStack } from "@chakra-ui/react";
import { GameObject } from "../hooks/useGames";
import PlatformIconList from "./PlatformIconList";
import CriticScore from "./CriticScore";
import getCroppedImageURL from "../services/getCroppedImageURL";
//import GameGenreDisplay from "./GameGenresDisplay";
//import getEmoji from "./Emoji";
import Emoji from "./Emoji";

interface Props {
  game: GameObject;
}

const GameCard = ({ game }: Props) => {
  return (
    <Card borderRadius={10} overflow="hidden">
      <Image src={getCroppedImageURL(game.background_image)}></Image>
      <CardBody>
        <HStack justifyContent={"space-between"}>
          <PlatformIconList
            platforms={game.parent_platforms.map((p) => p.platform)}
          />
          <CriticScore score={game.metacritic} />
        </HStack>
        <Heading paddingTop={1} fontSize="2xl">
          {game.name}
          <Emoji rating={game.rating_top} />
        </Heading>

        {/* <GameGenreDisplay genres={game.genres} /> */}
      </CardBody>
    </Card>
  );
};

export default GameCard;
