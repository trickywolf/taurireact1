import { Genre } from "../hooks/useGenres";
import { Text, HStack } from "@chakra-ui/react";

interface Props {
  genres: Genre[];
}

const GameGenreDisplay = ({ genres }: Props) => {
  return (
    <HStack justifyContent={"space-between"}>
      <div></div>
      <Text fontSize="sm" color={"gray.400"}>
        {genres.map((g) => g.name).join(", ")}
      </Text>
    </HStack>
  );
};

export default GameGenreDisplay;
