import useGenres, { Genre } from "../hooks/useGenres";
import {
  List,
  ListItem,
  HStack,
  Image,
  Spinner,
  Button,
  Heading,
} from "@chakra-ui/react";
import getCroppedImageURL from "../services/getCroppedImageURL";


interface Props {
  onSelectGenre: (genre: Genre) => void;
  selectedGenre: Genre | null;
}

const GenreList = ({ onSelectGenre, selectedGenre }: Props) => {
  const { data, error, isLoading } = useGenres();

  if (error) return null;
  return (
    <>
      {isLoading && <Spinner />}
      <Heading fontSize="2xl" marginBottom={2}>
        Genres
      </Heading>
      <List>
        {data.map((g) => (
          <ListItem paddingY="4px" key={g.id}>
            <HStack>
              <Image
                borderRadius={8}
                overflow="hidden"
                boxSize="32px"
                objectFit="cover"
                src={getCroppedImageURL(g.image_background)}
              ></Image>
              <Button
                variant="link"
                fontSize="larger"
                fontWeight={selectedGenre?.id === g.id ? "bold" : "normal"}
                onClick={() => onSelectGenre(g)}
                whiteSpace="normal"
                textAlign="left"
              >
                {g.name}
              </Button>{" "}
            </HStack>
          </ListItem>
        ))}
      </List>
    </>
  );
};

export default GenreList;
