import {
  
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  
} from "@chakra-ui/react";
import { BsChevronDown } from "react-icons/bs";

const orderMap = [
  { value: "", label: "Relevance" },
  { value: "-added", label: "Date Added" },
  { value: "name", label: "Name" },
  { value: "-released", label: "Release Date" },
  { value: "-metacritic", label: "Popularity" },
  { value: "-rating", label: "Average Rating" },
];

interface Props {
  onSelectOrder: (order: string) => void;
  selectedOrder: string;
}

const OrderSelector = ({ onSelectOrder, selectedOrder }: Props) => {
  const findLabelByValue = (value: string) => {
    const foundEntry = orderMap.find((order) => order.value === value);
    return foundEntry ? foundEntry.label : null;
  };

  return (
    <>
      <Menu>
        <MenuButton as={Button} rightIcon={<BsChevronDown />}>
          {" "}
          Order by:{" "}
          {selectedOrder ? findLabelByValue(selectedOrder) : "Relevance"}
        </MenuButton>
        <MenuList>
          {orderMap.map((order) => (
            <MenuItem
              key={order.value}
              onClick={() => onSelectOrder(order.value)}
            >
              {order.label}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
    </>
  );
};

export default OrderSelector;
