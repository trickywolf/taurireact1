import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,

  Spinner,
} from "@chakra-ui/react";

import usePlatforms, { Platform } from "../hooks/usePlatforms";
import { BsChevronDown } from "react-icons/bs";

interface Props {
  onSelectPlatform: (p: Platform) => void;
  selectedPlatform: Platform | null;
}

const PlatformSelector = ({ onSelectPlatform, selectedPlatform }: Props) => {
  const { data, error, isLoading } = usePlatforms();

  if (error) return null;
  return (
    <>
      {isLoading && <Spinner />}
      {!isLoading && (
        <Menu>
          <MenuButton as={Button} rightIcon={<BsChevronDown />}>
            {" "}
            {selectedPlatform ? selectedPlatform.name : "Platforms"}
          </MenuButton>
          <MenuList>
            {data.map((platform) => (
              <MenuItem
                key={platform.id}
                onClick={() => {
                  onSelectPlatform(platform);
                  console.log("platform click");
                }}
              >
                {platform.name}
              </MenuItem>
            ))}
          </MenuList>
        </Menu>
      )}
    </>
  );
};

export default PlatformSelector;
