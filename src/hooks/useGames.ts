import { Genre } from "./useGenres";
import useData from "./useData";
import { Platform } from "./usePlatforms";
import { GameQuery } from "../App";


export interface GameObject {
    id: number;
    name: string;
    background_image:string;
    parent_platforms: {platform:Platform}[];
    metacritic:number
    genres: Genre[];
    rating_top:number;
  }

const useGames=(gameQuery:GameQuery)=>
useData<GameObject>("/games"
,{
  params:{
    genres:gameQuery.genre?.id
    ,platforms:gameQuery.platform?.id,
    ordering:gameQuery.order,
    search:gameQuery.searchText
}}
,[gameQuery]);
  

export default useGames;