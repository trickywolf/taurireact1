import noImage from "../assets/no-image-placeholder-6f3882e0.webp"

const getCroppedImageURL=(src:string)=>{
    if (!src) return noImage;
    const newSrc = src.replace("/media/", "/media/crop/600/400/");
    return newSrc;
}

export default getCroppedImageURL;